package org.ricardorm13.entities;

import io.quarkus.runtime.annotations.RegisterForReflection;
import org.ricardorm13.repository.AbstractService;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;
import java.util.Objects;

@RegisterForReflection
public class PrecosCDB {
    private String date;
    private String unitPrice;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PrecosCDB)) {
            return false;
        }

        PrecosCDB other = (PrecosCDB) obj;

        return Objects.equals(other.date, this.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.date);
    }
}
