package org.ricardorm13.services;

import org.ricardorm13.entities.PrecosCDI;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CalculoTCDI {
    private List<PrecosCDI> listaIndices = new ArrayList<>();

    public List<PrecosCDI> calcularTCDI (List<PrecosCDI> lista){

        for (PrecosCDI preco : lista){
            DecimalFormat df = new DecimalFormat("###.########");
            Double price = (Double.parseDouble(preco.getLastTradePrice())/100) + 1;
            Double exp = Double.valueOf(1f /252f);
            BigDecimal tcdi = new BigDecimal(df.format(Math.pow(price, exp)));
            preco.setTCDI(tcdi.subtract(new BigDecimal(1)));
            listaIndices.add(preco);
        }
        listaIndices.sort(PrecosCDI::compareTo);

        return listaIndices;
    }
}
