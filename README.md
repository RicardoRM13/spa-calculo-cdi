# SPA-Calculo-CDI

Esse projeto possui todos os artefatos necessários para build e deploy ou apenas deploy de um site que efetua cálculo de CDI.

A solução foi criada com base nos serviços de cloud pública da AWS:

![Screenshot](Untitled Diagram.png)

- DynamoDB - Banco de dados para armazenar as taxas do CDI utilizadas para os cáculos.
- Lambda Function - Lambdas utilizadas para carga das taxas do CDI e consulta no DynamoDB. Uma das lambdas foi construda com framework Quarkus para geraço de um Native Image, tornando o cold start e execuçoes extremanente rapidos.
- API Gateway - Exposição da API utilizada pelo site para cálculo do CDI.
- Bucket S3 - Utilizado para disponibilização do site SPA em Angular, armazenamento dos binários para as Lambdas e armazenamento do CSV utilizado para carga das taxas CDI.

Estrutua do repositório:

Pastas:
- lambda-calculo-cdi - Código fonte da lambda utilizada para calculo do CDI, construída com framework Quarkus e que  executada com Native Image. Para saber como    buildar uma Native Image, acesse: https://quarkus.io/guides/building-native-image.
- lambda-carga-cdi - Cdigo fonte da lambda utilizada para fazer a carga da planilha com historico de taxas no DynamoDB
- SPA - Código do site SPA que deverá ser colocado no bucket S3
- IaaC - cloudFormation para criação dos serviços na AWS.
- Dist - Arquivos zip com as lambdas e o swagger para criação da API.


Arquivo CSV com o histórico de taxas do CDI para carga no DynamoDB.

**Instruções para o deploy:**

**Requisitos:**

- Conta existente na AWS
- Arquivo CSV com o layout do arquivo existente nesse repositório.


A criação dos serviços pode ser feita com aws cli ou aws console, aqui passerei as instruções utilizando o AWS Console.

1. Acesso o AWS Console no link: [](https://aws.amazon.com/console/)
2. Selecione "**Services**" e depois digite **S3** na busca.
3. Neste momento faremos a criação de um Bucket para colocar os arquivos da pasta Dist, pode escolher o nome que quiser.
4. Faça o upload dos arquivos na raiz do bucket.
5. Selecione "**Services**" novamente e digite **cloudFormation** na busca.
6. Após abrir a página do serviço cloudFormation, clique em "**Create Stack**" e selecione **With new resources**.
7. Após mudar de tela, clique em **Upload a template file** e clique em **Choose a File**, selecione o arquivo cloudFormation que está dentro da pasta **IaaC** e clique em **Next**.
8. Após carregar a tela, você precisará preencher 4 campos:
-     Stack Name: Coloque o nome que desejar
-     BucketArtfactsName: Coloque o nome do bucket criado no passo 3.
-     BucketName: Coloque um nome para o bucket que será criado para posterior armazenamento do csv com as taxas CDI.
-     File: Coloque o nome do arquivo CSV, que deverá ser usado para carga das taxas CDI.
9. Após isso vá clicando em **Next** até começar a execução da stack.

Após a execução da stack, serão criados os serviços abaixo:
- Bucket S3 para armazenamento do CSV, esse bucket tem uma trigger e toda vez que um CSV é incluído nele é executada a lambda que carrega os dados no DynamoDB.
- DynamoDB com uma tabela chamada Indices.
- Uma Lambda para fazer a carga no DynamoDB.
- Uma Lambda para calcular o CDI acumulado.
- API Gateway configurado com a API para calculo.

Agora iremos configurar o site SPA no bucket S3 que irá serviço como um site.

Um requisito para o site é ter o endereço do **API Gateway** que foi criado, para obtê-lo acesso o serviço API Gateway, depois em **APIs**, **Stages** e depois clique em cima do stage **Dev**, 
copie o endereço que aparece a frente de **Invoke URL****, ele deve se parecer com essa: https://2rilloudsd.execute-api.sa-east-1.amazonaws.com/dev
Abra o arquivo chamado **controller.js** que está dentro da pasta **/SPA/app/controllers** e altere a variavel api, colocando o endereço do gateway obtido acima.

1. Acesse o serviço S3.
2. Clique em **Create Bucket**, será aberto um popup, coloque um nome no campo **Bucket Name**.
3. Clique em **Next**, depois em **Next** novamente, role a tela para baixo e escolha a opção **Grant Amazon S3 Log Delivery** e clique em **Next** e depois em **Create Bucket**.
4. Após carregar a tela com a lista de buckets, clique no bucket criado, depois clique em **Properties** e depois em **Static website hosting**.
5. Nessa caixa, clique na opção **Use this bucket to host a website** e digite o nome **index.html** no campo **Index document**, clique em **Save**.
6. Clique em **Overview**, depois em **upload**, selecione todos os arquivos que estão na pasta SPA e os envie na mesma estrutura que estiver na pasta, o arquivo **index.html** deve ficar na raiz do bucket.

Agora precisaremos criar uma policy para restringir o acesso à ele e poder tornar os arquivos publicos para que possa ser acessado. 
EM Permissions - Bucket Policy inclua a policy abaixo:

```
{
    "Version": "2012-10-17",
    "Id": "ExamplePolicy01",
    "Statement": [
        {
            "Sid": "ExampleStatement01",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<sua_conta>:user/<seu_usuario>"
            },
            "Action": [
                "s3:*"
            ],
            "Resource": [
                "arn:aws:s3:::<nome_do_bucket>/*"
            ]
        }
    ]
}
```
Feito isso, volte clique em **Block Public Acces**s clique em editar e desmarque a opção **Block all public access**

Agora volte na raíz do bucket, selecione o arquivo index.html e todas as pastas, depois clique em **Actions** e **Make Public**.

***Pronto!!***

Para saber o endereço do site basta clicar no arquivo index.html, ele exibirá o endereço completo, só clicar!

