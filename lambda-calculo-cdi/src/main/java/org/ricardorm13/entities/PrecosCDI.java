package org.ricardorm13.entities;

import org.ricardorm13.repository.AbstractService;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;

@RegisterForReflection
public class PrecosCDI implements Comparable<PrecosCDI> {
    private String SecurityName;
    private String DataTaxa;
    private String LastTradePrice;
    private BigDecimal TCDI;

    public PrecosCDI(){

    }

    public PrecosCDI(String SecurityName, String DataTaxa, String LastTradePrice){
        this.SecurityName = SecurityName;
        this.DataTaxa = DataTaxa;
        this.LastTradePrice = LastTradePrice;
    }


    public String getSecurityName() {
        return SecurityName;
    }

    public void setSecurityName(String nome) {
        this.SecurityName = nome;
    }


    public String getDataTaxa() {
        return DataTaxa;
    }

    public void setDataTaxa(String dataTaxa) {
        this.DataTaxa = dataTaxa;
    }

    public String getLastTradePrice() {
        return LastTradePrice;
    }

    public void setLastTradePrice(String lastTradePrice) {
        this.LastTradePrice = lastTradePrice;
    }

    public static PrecosCDI from(Map<String, AttributeValue> item) {
        PrecosCDI precosCDI = new PrecosCDI();
        if (item != null && !item.isEmpty()) {
            precosCDI.setSecurityName(item.get(AbstractService.SecurityName).s());
            precosCDI.setDataTaxa(item.get(AbstractService.DataTaxa).s());
            precosCDI.setLastTradePrice(item.get(AbstractService.LastTradePrice).s());
        }
        return precosCDI;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PrecosCDI)) {
            return false;
        }

        PrecosCDI other = (PrecosCDI) obj;

        return Objects.equals(other.DataTaxa, this.DataTaxa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.SecurityName);
    }

    public BigDecimal getTCDI() {
        return TCDI;
    }

    public void setTCDI(BigDecimal TCDI) {
        this.TCDI = TCDI;
    }

    @Override
    public int compareTo(PrecosCDI precosCDI) {
        return this.getDataTaxa().compareTo(precosCDI.DataTaxa);
    }
}

