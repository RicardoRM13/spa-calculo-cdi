package org.ricardorm13.services;

import org.ricardorm13.repository.AbstractService;
import org.ricardorm13.entities.PrecosCDB;
import org.ricardorm13.entities.PrecosCDI;
import org.ricardorm13.entities.RequestEvent;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class CalculoService extends AbstractService {

    @Inject
    DynamoDbClient dynamoDbClient;

    public List<PrecosCDB> listaTaxas(RequestEvent req) {
        CalculoTCDI calculoTCDI = new CalculoTCDI();
        CalculoCDB calculoCDB = new CalculoCDB();
        return calculoCDB.calcularCDB(calculoTCDI.calcularTCDI(dynamoDbClient.scanPaginator(scanRequest(req.getDataInicial(), req.getDataFinal())).items().stream()
                    .map(PrecosCDI::from)
                    .collect(Collectors.toList())),req.getTaxa());
    }

}
