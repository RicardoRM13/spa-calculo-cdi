package org.ricardorm13.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class RequestEvent {
    private Map<String, Object> queryStringParameters;
    private String dataInicial;
    private String dataFinal;
    private Float taxa;

    public Map<String, Object> getQueryStringParameters() {
        return this.queryStringParameters;
    }

    public void setQueryStringParameters(Map<String, Object> queryParam) {

        this.queryStringParameters = queryParam;
        this.dataInicial = this.getQueryStringParameters().get("investmentDate").toString();
        this.dataFinal = this.getQueryStringParameters().get("currentDate").toString();
        this.taxa = Float.parseFloat(getQueryStringParameters().get("cdbRate").toString());
    }

    public String getDataInicial() {

        return dataInicial;
    }

    public void setDataInicial(String dataInicial) {

        this.dataInicial = dataInicial;
    }

    public String getDataFinal() {

        return dataFinal;
    }

    public void setDataFinal(String dataFinal) {

        this.dataFinal = dataFinal;
    }

    public Float getTaxa() {
        return taxa;
    }

    public void setTaxa(Float taxa) {
        this.taxa = taxa;
    }
}
