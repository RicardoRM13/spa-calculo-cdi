package org.ricardorm13.services;

import org.ricardorm13.entities.PrecosCDB;
import org.ricardorm13.entities.PrecosCDI;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CalculoCDB {
    private List<PrecosCDB> precosCDBList = new ArrayList<>();

    public List<PrecosCDB> calcularCDB(List<PrecosCDI> listaCDI, Float taxa){
        DecimalFormat df2 = new DecimalFormat("#########.##");
        DecimalFormat df8 = new DecimalFormat("###.########");

        BigDecimal bTaxa = new BigDecimal(taxa);

        BigDecimal precoAnterior = new BigDecimal(1.0);
        for (PrecosCDI precosCDI : listaCDI){
            BigDecimal cdia = precosCDI.getTCDI().multiply(bTaxa.divide(new BigDecimal(100))).add(new BigDecimal(1));

            precoAnterior = precoAnterior.multiply(cdia);

            BigDecimal pUnitario = new BigDecimal(1000.00);
            pUnitario = pUnitario.multiply(new BigDecimal(df8.format(precoAnterior)));

            PrecosCDB precosCDB = new PrecosCDB();
            precosCDB.setUnitPrice(df2.format(pUnitario));
            precosCDB.setDate(precosCDI.getDataTaxa());

            precosCDBList.add(precosCDB);

        }

        if (precosCDBList.size() > 0) { precosCDBList.remove(precosCDBList.size()-1); }



        return precosCDBList;
    }
}
