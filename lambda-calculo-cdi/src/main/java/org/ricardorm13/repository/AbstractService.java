package org.ricardorm13.repository;


import java.util.HashMap;
import java.util.Map;

import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;
import sun.awt.X11.XSystemTrayPeer;

public abstract class AbstractService {

    public final static String DataTaxa = "DataTaxa";
    public final static String SecurityName = "SecurityName";
    public final static String LastTradePrice = "LastTradePrice";

    public String getTableName() {
        return "Indices";
    }

    protected ScanRequest scanRequest(String dataInic, String dataFim) {
        Map<String, AttributeValue> key = new HashMap<>();
        key.put(":dataInic", AttributeValue.builder().s(dataInic).build());
        key.put(":dataFim", AttributeValue.builder().s(dataFim).build());
        System.out.println("Tabela " + getTableName()) ;
        System.out.println("DataTaxa between " + dataInic + " and " + dataFim) ;
        return ScanRequest.builder().tableName(getTableName())
                .filterExpression("DataTaxa BETWEEN :dataInic AND :dataFim")
                .expressionAttributeValues(key).build();

    }


}