package org.ricardorm13;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.json.JSONArray;
import org.ricardorm13.entities.RequestEvent;
import org.ricardorm13.services.CalculoService;
import org.ricardorm13.util.ApiGatewayResponse;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;

@Named("precosCDI")
public class LambdaCalculo implements RequestHandler<RequestEvent, ApiGatewayResponse> {

    @Inject
    CalculoService calculoService;

    public ApiGatewayResponse handleRequest(RequestEvent requestEvent, Context context) {

        System.out.println("Iniciando lambda: " + requestEvent);

        JSONArray json = null;
        json = new JSONArray(calculoService.listaTaxas(requestEvent));
        return ApiGatewayResponse.builder()
                .setStatusCode(200)
                .setRawBody(json.toString())
                .setHeaders(Collections.singletonMap("Access-Control-Allow-Origin", "*"))
                .build();
    }
}
