
app.controller('MainController', ['$scope', '$http', 'SimplePaginate', function($scope, $http, SimplePaginate) {
	
	

	$scope.calcular = function() {
		
		const api = "https://tctd0fipw0.execute-api.sa-east-1.amazonaws.com/dev/precos-cdb"
		
		var dataInic = formataData($scope.datInic)
		var dataFim = formataData($scope.datFim)
	    
		var baseUrl =  api + '?investmentDate=' + dataInic + '&currentDate=' + dataFim + '&cdbRate=' + $scope.taxaCDB;
		
		$scope.showbar = true

		progressBar(15,0)

		$http({
			method: "GET",
			url: baseUrl,
			dataType: 'json',
			headers: { "Content-Type": "application/json" }
		}).then(successCallback, errorCallback);
			
			
		function successCallback(response){
			$scope.valorFinal = response.data.pop()	
			
			
			progressBar(30,0)
			
			
			$scope.lista = true

			SimplePaginate.configure({
				data: response.data,
				perPage: 23
			});
			
			progressBar(60,0)
			$scope.paginate = {
				result : SimplePaginate.goToPage(0),
				total : SimplePaginate.itemsTotal(),
				next : function() {
					$scope.paginate.result = SimplePaginate.next();
				},
				prev : function() {
					$scope.paginate.result = SimplePaginate.prev();
				}
			};

			
			progressBar(90,0)
			carregarGrafico(response.data)

			
		}
		function errorCallback(error){
			$scope.lista = false
		}
	}

	function progressBar(tempo,i){
		if (i == 0) {
			i = 1;
			var elem = document.getElementById("myBar");
			var width = 1;
			var id = setInterval(frame, 1);
			function frame() {
			if (width >= tempo) {
				clearInterval(id);
					i = 0;
				} else {
					width++;
					elem.style.width = width + "%";
				}
			}
		}
		
	}

	function formataData(data){
		var dia = data.getDate()
		if (dia.toString().length == 1){
			dia = "0".concat(dia)
		}
		var mes = data.getMonth() + 1
		if (mes.toString().length == 1){
			mes = "0".concat(mes)
		}
		var ano = data.getFullYear()

		return ano + "-" + mes + "-" + dia
	}

	function obtemDatas(item, index) {
		
		$scope.labels.push(item.date)
		$scope.data.push(item.unitPrice)
	}

	function carregarGrafico(precos){
		$scope.labels = []
		$scope.data = []
		precos.forEach(obtemDatas);


		
		$scope.series = ['Series A'];
				
		$scope.onClick = function (points, evt) {
			console.log(points, evt);
		};
		$scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
		$scope.options = {
			responsive: true,
				title: {
					display: true,
					text: 'Valores CDI Acumulado'
				},
				tooltips: {
					mode: 'index'
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true
						},
						ticks: {
							maxTicksLimit: 10 
						}
					}],
					yAxes: [{
						id: 'y-axis-1',
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						},
						ticks: {
							suggestedMin: 1000,
							suggestedMax: 200,
							stepSize: 1,
							maxTicksLimit: 10 
						}
					}]
				}
		};
	}

	
	
	
  }]);